# docker-images-aws

Some docker images for AWS tools.

## aws-cli

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-aws/aws-cli:latest
```

An official image is available on [docker hub](https://hub.docker.com/r/amazon/aws-cli).
The problem with it is the entrypoint, that is defined as the aws executable.
That means that using it as image in the CI seems not possible.

As example:
```shell
docker run -it <Image ID>
```
Above command will not work.

This image is based on [the AWS Docker file](https://github.com/aws/aws-cli/blob/v2/docker/Dockerfile).
